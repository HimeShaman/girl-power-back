import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { RegisterDto } from './register.dto';
import { Profile } from '../profile/profile.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Profile)
    private profileRepository: Repository<Profile>,
  ) {}

  async create(registerInfos: RegisterDto) {
    if (await this.userRepository.findOne({ email: registerInfos.email })) {
      throw new ConflictException('This email is already in use.');
    }

    const newProfile = await this.profileRepository.save({
      firstname: registerInfos.firstname,
      lastname: registerInfos.lastname,
    });

    const hashPass = await bcrypt.hash(registerInfos.password, 10);

    await this.userRepository.save({
      email: registerInfos.email,
      password: hashPass,
      profile: newProfile,
    });
  }
}
