export type RegisterDto = {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
};
