export type UserDto = {
  id: number;
  email: string;
};
