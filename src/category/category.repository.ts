import { EntityRepository, ILike, Like, Repository } from 'typeorm';
import { Category } from './category.entity';

@EntityRepository(Category)
export class CategoryRepository extends Repository<Category> {
  async findByCriterias(criterias: categoryCriterias): Promise<Category[]> {
    const query: any = {};
    if (criterias.name) {
      query.name = ILike(`%${criterias.name}%`);
    }
    return this.find(query);
  }
}

export type categoryCriterias = {
  name?: string;
};
