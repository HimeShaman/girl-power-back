import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Charity } from '../charity/charity.entity';
import { Tag } from '../tag/tag.entity';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  hex: string;

  @OneToMany(() => Charity, (charity) => charity.category)
  charities: Charity[];
}
