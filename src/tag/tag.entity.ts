import { Entity, Column, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';
import { Charity } from '../charity/charity.entity';

@Entity()
export class Tag {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  hex: string;

  @ManyToMany((type) => Charity)
  charities: Charity[];
}
