import { EntityRepository, ILike, Repository } from 'typeorm';
import { Tag } from './tag.entity';

@EntityRepository(Tag)
export class TagRepository extends Repository<Tag> {
  async findByCriterias(criterias: tagCriterias): Promise<Tag[]> {
    const query: any = {};
    if (criterias.name) {
      query.name = ILike(`%${criterias.name}%`);
    }
    return this.find(query);
  }
}

export type tagCriterias = {
  name?: string;
};
