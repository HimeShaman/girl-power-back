import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { CharityModule } from './charity/charity.module';
import { Charity } from './charity/charity.entity';
import { CategoryModule } from './category/category.module';
import { Category } from './category/category.entity';
import { TagModule } from './tag/tag.module';
import { Tag } from './tag/tag.entity';
import { SmsModule } from './sms/sms.module';
import { DonationModule } from './donation/donation.module';
import { Donation } from './donation/donation.entity';
import { Message } from './donation/message/message.entity';
import { UserModule } from './user/user.module';
import { User } from './user/user.entity';
import { ProfileModule } from './profile/profile.module';
import { Profile } from './profile/profile.entity';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    CharityModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: (process.env.DATABASE_PORT as unknown) as number,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE,
      entities: [Charity, Category, Tag, Donation, Message, User, Profile],
      synchronize: true,
    }),
    CategoryModule,
    TagModule,
    SmsModule,
    DonationModule,
    UserModule,
    ProfileModule,
    AuthModule,
  ],
})
export class AppModule {}
