export type SmsDto = {
  sender: string;
  message: string;
};

export type SmsResponseDto = {
  message: string;
};
