import { Body, Controller, Post } from '@nestjs/common';
import { SmsService } from './sms.service';
import { SmsDto } from './sms.dto';

@Controller('api/sms')
export class SmsController {
  constructor(private readonly smsService: SmsService) {}

  @Post('notify')
  processSms(@Body() sms: SmsDto) {
    return this.smsService.processSms(sms);
  }
}
