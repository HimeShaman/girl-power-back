import { BadRequestException, Injectable } from '@nestjs/common';
import { SmsDto, SmsResponseDto } from './sms.dto';
import { DonationService } from '../donation/donation.service';

@Injectable()
export class SmsService {
  constructor(private readonly donationService: DonationService) {}

  async processSms(sms: SmsDto): Promise<SmsResponseDto> {
    const donationValue = this.validateSms(sms);
    const message = await this.donationService.createDonationFromSms(
      sms.sender,
      donationValue,
    );
    return { message };
  }

  private validateSms(sms: SmsDto): number {
    if (!sms.sender) {
      throw new BadRequestException('The sender was not found.');
    }

    if (!sms.message || !sms.message.includes(process.env.DONATION_TAG)) {
      throw new BadRequestException('The donation tag was not found.');
    }
    const donationValue: string = sms.message
      .trim()
      .replace(process.env.DONATION_TAG, '');

    if (Number.isNaN(donationValue)) {
      throw new BadRequestException('The donation value is not a number.');
    }

    return Number(donationValue);
  }
}
