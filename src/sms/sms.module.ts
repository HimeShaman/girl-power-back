import { Module } from '@nestjs/common';
import { SmsController } from './sms.controller';
import { SmsService } from './sms.service';
import { DonationModule } from '../donation/donation.module';

@Module({
  imports: [DonationModule],
  controllers: [SmsController],
  providers: [SmsService],
})
export class SmsModule {}
