import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';
import { Category } from '../category/category.entity';
import { Tag } from '../tag/tag.entity';
import { Donation } from '../donation/donation.entity';
import { Message } from '../donation/message/message.entity';

@Entity()
export class Charity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  priority: number;

  @Column({ nullable: true })
  priorityReason?: string;

  @Column({ name: 'image_url', nullable: true })
  imageUrl?: string;

  @Column({ name: 'creation_date', nullable: true })
  creationDate?: Date;

  @Column({ nullable: true })
  website?: string;

  @Column({ nullable: true })
  area: string;

  @ManyToOne(() => Category, (category) => category.charities)
  category: Category;

  @ManyToMany((type) => Tag)
  @JoinTable()
  tags: Tag[];

  @OneToMany(() => Donation, (donation) => donation.charity)
  donations: Donation[];

  @OneToMany(() => Message, (message) => message.charity)
  messages: Message[];
}
