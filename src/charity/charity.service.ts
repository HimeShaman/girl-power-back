import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Charity } from './charity.entity';
import { CharityRepository } from './charity.repository';

@Injectable()
export class CharityService {
  constructor(
    @InjectRepository(Charity)
    private charityRepository: CharityRepository,
  ) {}

  async findAll(term: string, category: string): Promise<Charity[]> {
    return this.charityRepository.findByCriterias({ term, category });
  }

  async findById(id: string): Promise<Charity> {
    const charity = await this.charityRepository.findOne(id, {
      relations: ['category'],
    });
    if (!charity) {
      throw new NotFoundException(`Charity ${id} was not found.`);
    }
    return charity;
  }
}
