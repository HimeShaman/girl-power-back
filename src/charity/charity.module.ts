import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Charity } from './charity.entity';
import { CharityService } from './charity.service';
import { CharityController } from './charity.controller';
import { CharityRepository } from './charity.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Charity, CharityRepository])],
  providers: [CharityService],
  controllers: [CharityController],
})
export class CharityModule {}
