import { Controller, Get, Param, Query } from '@nestjs/common';
import { CharityService } from './charity.service';

@Controller('charity')
export class CharityController {
  constructor(private readonly charityService: CharityService) {}

  @Get()
  findAll(@Query('term') term: string, @Query('category') category: string) {
    return this.charityService.findAll(term, category);
  }

  @Get(':id')
  findById(@Param('id') charityId: string) {
    return this.charityService.findById(charityId);
  }
}
