import { EntityRepository, Repository } from 'typeorm';
import { Charity } from './charity.entity';

@EntityRepository(Charity)
export class CharityRepository extends Repository<Charity> {
  async findTop3Prioritised(): Promise<Charity[]> {
    return this.createQueryBuilder('charity')
      .orderBy('charity.priority', 'DESC')
      .take(3)
      .getMany();
  }

  async findEmergency(): Promise<Charity> {
    return this.createQueryBuilder('charity')
      .where('charity.priority = 99')
      .getOne();
  }

  async findByCriterias(criterias: charityCriterias): Promise<Charity[]> {
    const queryBuilder = this.createQueryBuilder('charity')
      .innerJoinAndSelect('charity.category', 'category')
      .where('1=1');

    if (criterias.term) {
      queryBuilder
        .andWhere('charity.name ILIKE :term')
        .setParameter('term', `%${criterias.term}%`);
    }
    if (criterias.category) {
      queryBuilder
        .andWhere('category.name ILIKE :category')
        .setParameter('category', `%${criterias.category}%`);
    }
    return queryBuilder.getMany();
  }
}

export type charityCriterias = {
  term: string;
  category: string;
};
