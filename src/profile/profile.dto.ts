export type ProfileDto = {
  email: string;
  firstname: string;
  lastname: string;
  adress: string;
  phone: string;
};

export type CreditCardDto = {
  number: string;
  name: string;
  cryptogram: string;
  expirationDate: string;
};
