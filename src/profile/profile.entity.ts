import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { User } from '../user/user.entity';

@Entity()
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstname: string;

  @Column()
  lastname: string;

  @Column({ nullable: true })
  adress: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ name: 'card_number', nullable: true })
  cardNumber: string;

  @Column({ name: 'card_name', nullable: true })
  cardName: string;

  @Column({ name: 'card_cryptogram', nullable: true })
  cardCryptogram: string;

  @Column({ name: 'card_expiration_date', nullable: true })
  cardExpirationDate: string;

  @OneToMany(() => User, (user) => user.profile, { nullable: true })
  users?: User[];
}
