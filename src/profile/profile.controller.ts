import { Body, Controller, Get, Put, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { User } from '../auth/user.decorator';
import { UserDto } from '../user/userDto';
import { ProfileService } from './profile.service';
import { CreditCardDto, ProfileDto } from './profile.dto';

@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @UseGuards(JwtAuthGuard)
  @Get('')
  get(@User() user: UserDto) {
    return this.profileService.getByUserId(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Put('')
  update(@User() user: UserDto, @Body() newProfile: ProfileDto) {
    return this.profileService.update(user.id, newProfile);
  }

  @UseGuards(JwtAuthGuard)
  @Put('/creditcard')
  updateCard(@User() user: UserDto, @Body() newCard: CreditCardDto) {
    return this.profileService.updateCard(user.id, newCard);
  }
}
