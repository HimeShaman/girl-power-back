import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Profile } from './profile.entity';
import { User } from '../user/user.entity';
import { CreditCardDto, ProfileDto } from './profile.dto';

@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(Profile)
    private profileRepository: Repository<Profile>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async getByUserId(id: number) {
    return this.userRepository.findOne(id, { relations: ['profile'] });
  }

  async update(id: number, profile: ProfileDto) {
    const user = await this.userRepository.findOne(id, {
      relations: ['profile'],
    });
    user.email = profile.email;
    await this.userRepository.save(user);
    user.profile.lastname = profile.lastname;
    user.profile.firstname = profile.firstname;
    user.profile.adress = profile.adress;
    user.profile.phone = profile.phone;
    await this.profileRepository.save(user.profile);
  }

  async updateCard(id: number, creditCard: CreditCardDto) {
    const user = await this.userRepository.findOne(id, {
      relations: ['profile'],
    });
    user.profile.cardName = creditCard.name;
    user.profile.cardNumber = creditCard.number;
    user.profile.cardCryptogram = creditCard.cryptogram;
    user.profile.cardExpirationDate = creditCard.expirationDate;
    await this.profileRepository.save(user.profile);
  }
}
