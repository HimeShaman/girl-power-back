import { Module } from '@nestjs/common';
import { DonationController } from './donation.controller';
import { DonationService } from './donation.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Charity } from '../charity/charity.entity';
import { CharityRepository } from '../charity/charity.repository';
import { Donation } from './donation.entity';
import { DistributorService } from './distributor.service';
import { MessageService } from './message/message.service';
import { MessageRepository } from './message/message.repository';
import { User } from '../user/user.entity';
import { DonationRepository } from './donation.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Donation,
      Charity,
      User,
      CharityRepository,
      MessageRepository,
      DonationRepository,
    ]),
  ],
  controllers: [DonationController],
  providers: [DonationService, DistributorService, MessageService],
  exports: [DonationService],
})
export class DonationModule {}
