import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { DonationService } from './donation.service';
import { DistributorService } from './distributor.service';
import { CreateDonationRequestDto } from './donation.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { User } from '../auth/user.decorator';
import { UserDto } from '../user/userDto';

@Controller('donation')
export class DonationController {
  constructor(
    private readonly donationService: DonationService,
    private readonly distributorService: DistributorService,
  ) {}

  @Get('/distribution')
  findDistribution(@Query('value') value: number) {
    return this.distributorService.prepareDonations(value);
  }

  @Post()
  async create(@Body() donations: CreateDonationRequestDto) {
    await this.donationService.createDonations(
      donations.donations,
      'web',
      donations.sender,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get('')
  async findUserDonation(@User() user: UserDto) {
    return this.donationService.findByUserId(user.id);
  }
}
