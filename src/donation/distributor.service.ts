import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Charity } from '../charity/charity.entity';
import { CharityRepository } from '../charity/charity.repository';
import { DistributorDto } from './distributor.dto';

@Injectable()
export class DistributorService {
  constructor(
    @InjectRepository(Charity)
    private charityRepository: CharityRepository,
  ) {}

  async prepareDonations(donation_value: number) {
    const charities: Charity[] = await this.charityRepository.findTop3Prioritised();
    const distribution: DistributorDto[] = [];
    const emergency: Charity = await this.charityRepository.findEmergency();

    if (!donation_value) {
      throw new BadRequestException('The donation value was not found.');
    }

    if (emergency) {
      this.fillDistribution(donation_value, distribution, [emergency], 1);
    } else {
      //Le don est entièrement dédié à 1 asso si <=20€
      if (donation_value <= 20) {
        this.fillDistribution(donation_value, distribution, charities, 1);
      }

      //Le don est partagé entre deux assos s'il est compris entre 20 et 30€
      if (donation_value > 20 && donation_value <= 30) {
        this.fillDistribution(donation_value, distribution, charities, 2);
      }

      //Le don est partagé entre trois assos s'il est >30€
      if (donation_value > 30) {
        this.fillDistribution(donation_value, distribution, charities, 3);
      }
    }
    return distribution;
  }

  fillDistribution(
    value: number,
    distribution_array: DistributorDto[],
    charities_list: Charity[],
    charity_nb: number,
  ) {
    let totalPriority = 0;
    for (let i = 0; i < charity_nb; i++) {
      totalPriority += charities_list[i].priority;
    }

    for (let i = 0; i < charity_nb; i++) {
      distribution_array.push({
        charity: {
          charityId: charities_list[i].id,
          charityName: charities_list[i].name,
          priorityMessage: charities_list[i].priorityReason,
          imageUrl: charities_list[i].imageUrl,
        },
        value: (charities_list[i].priority / totalPriority) * value,
      });
    }

    return distribution_array;
  }
}
