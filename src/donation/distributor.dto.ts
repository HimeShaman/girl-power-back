export type DistributorDto = {
  charity: CharityDistributorDto;
  value: number;
};

export type CharityDistributorDto = {
  charityId: number;
  charityName: string;
  priorityMessage?: string;
  imageUrl: string;
};
