import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Charity } from '../charity/charity.entity';
import { CharityRepository } from '../charity/charity.repository';
import { DistributorService } from './distributor.service';
import { DistributorDto } from './distributor.dto';
import { Donation } from './donation.entity';
import { v4 as uuidv4 } from 'uuid';
import { MessageService } from './message/message.service';
import { DonationRepository } from './donation.repository';
import { User } from '../user/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DonationService {
  constructor(
    @InjectRepository(Charity)
    private charityRepository: CharityRepository,
    @InjectRepository(Donation)
    private donationRepository: DonationRepository,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private readonly distributorService: DistributorService,
    private readonly messageService: MessageService,
  ) {}

  async createDonationFromSms(sender: string, value: number) {
    const pendingDonations = await this.distributorService.prepareDonations(
      value,
    );
    await this.createDonations(pendingDonations, 'sms', sender);

    return this.messageService.writeThankingMessage(pendingDonations, value);
  }

  async createDonations(
    distributions: DistributorDto[],
    type: 'sms' | 'web',
    sender: string,
  ) {
    const transactionId = uuidv4();
    for (const distribution of distributions) {
      const charity = await this.charityRepository.findOne(
        distribution.charity.charityId,
      );
      await this.donationRepository.save({
        charity: charity,
        type: type,
        date: new Date(),
        transaction: transactionId,
        value: distribution.value,
        sender: sender,
      });
    }
  }

  async findByUserId(id: number) {
    const user = await this.userRepository.findOne(id, {
      relations: ['profile'],
    });
    return await this.donationRepository.findByUserInfos(
      user.profile.phone,
      user.email,
    );
  }
}
