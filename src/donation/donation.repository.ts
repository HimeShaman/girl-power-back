import { EntityRepository, Repository } from 'typeorm';
import { Donation } from './donation.entity';

@EntityRepository(Donation)
export class DonationRepository extends Repository<Donation> {
  async findByUserInfos(phone: string, mail: string): Promise<Donation[]> {
    return this.createQueryBuilder('donation')
      .innerJoinAndSelect('donation.charity', 'charity')
      .where(`donation.sender = '${phone}'`)
      .orWhere(`donation.sender = '${mail}'`)
      .orderBy('donation.date', 'DESC')
      .getMany();
  }
}
