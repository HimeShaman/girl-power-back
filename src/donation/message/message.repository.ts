import { EntityRepository, Repository } from 'typeorm';
import { Message } from './message.entity';
import { DistributorDto } from '../distributor.dto';

@EntityRepository(Message)
export class MessageRepository extends Repository<Message> {
  async findCharityMessage(charityId: number, value: number): Promise<Message> {
    return this.createQueryBuilder('message')
      .where(`message.charityId = ${charityId}`)
      .andWhere(`message.minDonation < ${value}`)
      .orderBy('message.minDonation', 'DESC')
      .getOne();
  }

  async writeThanksMessage(donations: DistributorDto[], value: number) {
    let message = `Merci pour votre don de ${value}€ avec Carity. Grâce à vous, `;
    for (let i = 0; i < donations.length; i++) {
      const charityMessage = await this.findCharityMessage(
        donations[i].charity.charityId,
        donations[i].value,
      );

      if (i == donations.length - 1 && i != 0) {
        message +=
          'et ' +
          donations[i].charity.charityName +
          ' ' +
          charityMessage.content +
          '. ';
      } else if (donations.length == 1) {
        message +=
          donations[i].charity.charityName +
          ' ' +
          charityMessage.content +
          '. ';
      } else {
        message +=
          donations[i].charity.charityName +
          ' ' +
          charityMessage.content +
          ', ';
      }
    }
    return message;
  }
}
