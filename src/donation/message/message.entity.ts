import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Charity } from '../../charity/charity.entity';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Charity, (charity) => charity.messages)
  charity: Charity;

  @Column()
  content: string;

  @Column({
    type: 'decimal',
  })
  minDonation: number;
}
