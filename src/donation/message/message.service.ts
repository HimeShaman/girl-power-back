import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DistributorDto } from '../distributor.dto';
import { MessageRepository } from './message.repository';
import { Message } from './message.entity';

@Injectable()
export class MessageService {
  constructor(
    @InjectRepository(Message)
    private messageRepository: MessageRepository,
  ) {}

  writeThankingMessage(donations: DistributorDto[], value: number) {
    return this.messageRepository.writeThanksMessage(donations, value);
  }
}
