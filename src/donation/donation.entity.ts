import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Charity } from '../charity/charity.entity';

@Entity()
export class Donation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  sender: string;

  @Column({
    type: 'decimal',
  })
  value: number;

  @ManyToOne(() => Charity, (charity) => charity.donations)
  charity: Charity;

  @Column()
  transaction: string;

  @Column()
  type: 'sms' | 'web';

  @Column()
  date: Date;
}
