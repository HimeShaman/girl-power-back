import { DistributorDto } from './distributor.dto';

export type CreateDonationRequestDto = {
  sender: string;
  donations: DistributorDto[];
};
